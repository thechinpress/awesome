function ApiFetch() {
  const api_key = '59e41d5ff20958e417b47e1acc53eea3';

  const fetchPopularMovies = async () => {
    const response = await fetch(
      `https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=${api_key}`,
    );

    return response.json();
  };

  return {fetchPopularMovies};
}

export default ApiFetch;
