import AsyncStorage from '@react-native-async-storage/async-storage';

const storeData = async (key, items) => {
  try {
    await AsyncStorage.setItem(key, JSON.stringify(items));
  } catch (error) {
    console.log(error);
  }
};

const getData = async key => {
  try {
    const result = await AsyncStorage.getItem(key);
    const value = JSON.parse(result);
    if (value !== null) return value;
  } catch (error) {
    console.log(error);
  }
};

const removeData = key => {
  return AsyncStorage.removeItem(key);
};

export default {
  storeData,
  getData,
  removeData,
};
