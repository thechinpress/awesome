import React from 'react';
import {Text, TouchableOpacity, StyleSheet} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome';

import HomeScreen from '../Screens/HomeScreen';
import ChatScreen from '../Screens/ChatScreen';
import NotificationScreen from '../Screens/NotificationScreen';
import AccountScreen from '../Screens/AccountScreen';
import LoginScreen from '../Screens/LoginScreen';
import RegisterScreen from '../Screens/RegisterScreen';
import Search from '../components/Search';
import DetailScreen from '../Screens/DetailScreen';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const headerBackgroundColor = {
  headerStyle: {
    backgroundColor: '#0eb7f0',
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    textAlign: 'center',
  },

  headerRight: () => {
    <Text>Hello</Text>;
  },
};

const HomeNavigator = () => {
  return (
    <Stack.Navigator screenOptions={headerBackgroundColor}>
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{
          headerRight: () => <Search />,
        }}
      />
      <Stack.Screen name="Detail" component={DetailScreen} />
    </Stack.Navigator>
  );
};

const AccountNavigator = () => {
  return (
    <Stack.Navigator screenOptions={headerBackgroundColor}>
      <Stack.Screen name="Profile" component={AccountScreen} />
      <Stack.Screen name="Login" component={LoginScreen} />
      <Stack.Screen name="Register" component={RegisterScreen} />
    </Stack.Navigator>
  );
};

const NotificationNavigator = () => {
  return (
    <Stack.Navigator screenOptions={headerBackgroundColor}>
      <Stack.Screen name="Notification" component={NotificationScreen} />
    </Stack.Navigator>
  );
};

const ChatNavigator = () => {
  return (
    <Stack.Navigator screenOptions={headerBackgroundColor}>
      <Stack.Screen name="Chat" component={ChatScreen} />
    </Stack.Navigator>
  );
};

function AppNavigation(props) {
  return (
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: '#0eb7f0',
          },
        }}>
        <Tab.Screen
          Options={{
            headerStyle: {
              backgroundColor: '#0eb7f0',
            },
          }}
          name="Home"
          component={HomeNavigator}
          options={{
            tabBarIcon: ({color, size}) => (
              <Icon name="home" size={size} color={color} />
            ),
          }}
        />
        <Tab.Screen
          name="Chat"
          component={ChatNavigator}
          options={{
            tabBarIcon: ({color, size}) => (
              <Icon name="comments" size={size} color={color} />
            ),
          }}
        />
        <Tab.Screen
          name="Notification"
          component={NotificationNavigator}
          options={{
            tabBarIcon: ({color, size}) => (
              <Icon name="bell" size={size} color={color} />
            ),
          }}
        />
        <Tab.Screen
          name="Account"
          component={AccountNavigator}
          options={{
            tabBarIcon: ({color, size}) => (
              <Icon name="user" size={size} color={color} />
            ),
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {},
});

export default AppNavigation;
