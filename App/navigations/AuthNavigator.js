import React, {useEffect, useState, useContext} from 'react';
import {ActivityIndicator, View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';

import {createStackNavigator} from '@react-navigation/stack';

import LoginScreen from '../Screens/LoginScreen';
import RegisterScreen from '../Screens/RegisterScreen';
import Context from '../Auths/Context';
import Storage from '../Auths/Storage';

const Stack = createStackNavigator();

function AuthNavigator(props) {
  // const [isRegistered, setIsRegistered] = useState(false);
  const [isReady, setIsReady] = useState(false);

  const {isRegistered, setIsRegistered} = useContext(Context);

  const checkUserRegister = async () => {
    setIsReady(true);
    const result = await Storage.getData('userProfile');
    if (result) {
      setIsRegistered(true);
    }

    setIsReady(false);
  };

  useEffect(() => {
    checkUserRegister();
  }, []);

  if (isReady) {
    return (
      <View>
        <ActivityIndicator size="large" />
      </View>
    );
  }

  return (
    <NavigationContainer>
      <Stack.Navigator>
        {isRegistered ? (
          <Stack.Screen name="Login" component={LoginScreen} />
        ) : (
          <Stack.Screen name="Register" component={RegisterScreen} />
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default AuthNavigator;
