import React, {useState} from 'react';
import {Text, Modal, View, TouchableOpacity, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import AppButton from './AppButton';

import FormInput from './FormInput';

function Search({onPress}) {
  const [visible, setVisible] = useState(false);
  return (
    <>
      <TouchableOpacity
        style={styles.container}
        onPress={() => setVisible(true)}>
        <Icon name="search" color="#fff" size={20} />
      </TouchableOpacity>
      <Modal visible={visible} animationType="slide">
        <View style={styles.searchContainer}>
          <FormInput
            style={styles.input}
            placeholder="Search typing movie title"
          />
          <AppButton title="Search" onPress={onPress} />

          <TouchableOpacity
            style={styles.cancelContainer}
            onPress={() => setVisible(false)}>
            <Text style={styles.cancel}>Cancel</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingRight: 20,
  },

  searchContainer: {
    alignItems: 'center',
    margin: 20,
  },

  input: {
    borderBottomColor: 'lightgray',
    borderBottomWidth: 1,
    color: 'gray',
    width: '100%',
  },

  cancelContainer: {
    alignItems: 'center',
    backgroundColor: 'red',
    borderRadius: 10,
    marginTop: 20,
    padding: 10,
    width: 70,
  },

  cancel: {
    color: '#fff',
  },
});

export default Search;
