import React from 'react';
import {TouchableOpacity, Text, StyleSheet} from 'react-native';

function AppButton({title, onPress}) {
  return (
    <TouchableOpacity
      activeOpacity={0.4}
      onPress={onPress}
      style={styles.container}>
      <Text style={styles.login}>{title}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#0eb7f0',
    borderRadius: 40,
    justifyContent: 'center',
    marginTop: 20,
    width: '100%',
  },

  login: {
    color: '#fff',
    padding: 15,
  },
});

export default AppButton;
