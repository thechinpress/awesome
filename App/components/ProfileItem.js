import React from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

function ProfileItem({name, email, onPress}) {
  return (
    <View style={styles.container}>
      <View style={styles.profileIcon}>
        <Icon name="user" size={50} color="lightgray" />
      </View>
      <TouchableOpacity style={styles.details} onPress={onPress}>
        <Text style={styles.name}>{name}</Text>
        <Text style={styles.email}>{email}</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#fff',
    flexDirection: 'row',
    padding: 20,
  },
  profileIcon: {
    alignItems: 'center',
    borderColor: 'lightgray',
    borderWidth: 1,
    borderRadius: 50,
    height: 60,
    justifyContent: 'flex-end',
    width: 60,
  },
  details: {
    marginLeft: 10,
  },
  name: {
    fontSize: 17,
    fontWeight: '700',
  },

  email: {
    color: 'gray',
    fontStyle: 'italic',
    paddingTop: 3,
  },
});

export default ProfileItem;
