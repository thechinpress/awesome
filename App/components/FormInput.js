import React from 'react';
import {View, StyleSheet, TextInput} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

function FormInput({placeholder, icon, onChangeText, ...otherProps}) {
  return (
    <View style={styles.container}>
      {icon && (
        <Icon style={{paddingLeft: 10}} name={icon} size={25} color="gray" />
      )}
      <TextInput
        style={styles.input}
        {...otherProps}
        placeholder={placeholder}
        placeholderTextColor="gray"
        onChangeText={onChangeText}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#fff',
    borderRadius: 10,
    flexDirection: 'row',
    marginVertical: 10,
  },
  input: {
    paddingHorizontal: 10,
    color: '#333536',
    width: '90%',
  },
});

export default FormInput;
