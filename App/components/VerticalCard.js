import React from 'react';
import {Image, TouchableOpacity, Text, StyleSheet} from 'react-native';

function VerticalCard({title, image, onPress}) {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <Image style={styles.image} source={{uri: image}} />
      <Text style={styles.text}>{title}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    height: 160,
    width: 110,
    marginRight: 10,
    marginTop: 10,
  },

  image: {
    height: '100%',
    width: '100%',
  },
  text: {
    position: 'absolute',
    bottom: 10,
    color: '#f0f0f0',
    padding: 5,
  },
});

export default VerticalCard;
