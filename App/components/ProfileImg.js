import React from 'react';
import {View, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

function ProfileImg({name, size, color}) {
  return (
    <View style={styles.container}>
      <Icon name={name} size={size} color={color} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    borderRadius: 50,
    borderColor: 'gray',
    borderWidth: 1,
    height: 100,
    justifyContent: 'flex-end',
    left: '35%',
    position: 'absolute',
    top: 90,
    width: 100,
  },
});

export default ProfileImg;
