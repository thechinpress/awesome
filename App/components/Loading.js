import React from 'react';
import {ActivityIndicator, View, StyleSheet} from 'react-native';

function Loading({size}) {
  return (
    <View style={styles.container}>
      <ActivityIndicator size={size} color="#0eb7f0" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
});

export default Loading;
