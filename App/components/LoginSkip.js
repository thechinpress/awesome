import React from 'react';
import {Text, StyleSheet, TouchableOpacity} from 'react-native';

function LoginSkip({name, onPress}) {
  return (
    <TouchableOpacity style={styles.skip} onPress={onPress}>
      <Text>{name}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  skip: {
    position: 'absolute',
    top: 10,
    right: 20,
  },
});

export default LoginSkip;
