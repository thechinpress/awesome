import React from 'react';
import {Image, Text, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

function HorizontalCard({title, image, onPress}) {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <Image style={styles.image} source={{uri: image}} />

      <Text style={styles.text}>{title}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    borderRadius: 10,
    height: 160,
    marginRight: 10,
    overflow: 'hidden',
    width: 110,
  },

  image: {
    height: '100%',
    width: '100%',
  },

  text: {
    position: 'absolute',
    bottom: 10,
    color: '#fff',
    padding: 5,
  },
});

export default HorizontalCard;
