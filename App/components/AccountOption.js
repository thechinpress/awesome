import React from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';

function AccountOption({text, title, onPress}) {
  return (
    <View style={styles.container}>
      <Text>{text}</Text>
      <TouchableOpacity style={styles.loginOption} onPress={onPress}>
        <Text style={styles.loginBtn}>{title}</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginTop: 20,
  },

  loginOption: {
    paddingLeft: 20,
  },
  loginBtn: {
    color: '#0eb7f0',
  },
});

export default AccountOption;
