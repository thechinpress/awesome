import React, {useContext, useState} from 'react';
import {Text, View, StyleSheet, Alert, ActivityIndicator} from 'react-native';

import FormInput from '../components/FormInput';
import AppButton from '../components/AppButton';
import AccountOption from '../components/AccountOption';
import LoginSkip from '../components/LoginSkip';
import Context from '../Auths/Context';
import Storage from '../Auths/Storage';
import Loading from '../components/Loading';

function LoginScreen(props) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const {setIsRegistered, setSkip, setIsLogin} = useContext(Context);

  const handleSkip = async () => {
    setSkip(true);
  };

  const handleLogin = async () => {
    if (username === '' || password === '') {
      Alert.alert('Warning!', 'Username and password must be filled!', [
        {text: 'Ok'},
      ]);
      return;
    }
    // setIsLoading(true);
    const userLogin = true;

    await Storage.storeData('userLogin', userLogin);
    setIsLogin(userLogin);
    // setIsLoading(false);
  };

  const handleToRegister = () => {
    setIsRegistered(false);
  };

  return (
    <View style={styles.container}>
      <LoginSkip name="Skip" onPress={handleSkip} />
      <Text style={styles.login}>Login</Text>
      <FormInput
        placeholder="Username"
        icon="user"
        onChangeText={name => setUsername(name)}
      />
      <FormInput
        placeholder="Password"
        icon="lock"
        secureTextEntry={true}
        onChangeText={pass => setPassword(pass)}
      />
      {isLoading ? (
        <Loading size="small" />
      ) : (
        <AppButton title="Login" onPress={handleLogin} />
      )}

      <AccountOption
        text="Don't have an account?"
        title="Register"
        onPress={handleToRegister}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    margin: 15,
  },

  login: {
    fontSize: 25,
  },
});

export default LoginScreen;
