import React from 'react';
import {Image, View, Text, ScrollView, StyleSheet} from 'react-native';

function DetailScreen({route}) {
  const {item} = route.params;

  const imageUrl = 'https://image.tmdb.org/t/p/w500/';
  return (
    <ScrollView style={styles.container}>
      <Image style={styles.image} source={{uri: imageUrl + item.poster_path}} />

      <View style={styles.details}>
        <Text style={styles.title}>Title : {item.title}</Text>
        <Text>Release Date : {item.release_date}</Text>
        <Text>Vote Count : {item.vote_count}</Text>
        <Text>Vote Average : {item.vote_average}</Text>
        <Text>Popularity : {item.popularity}</Text>
      </View>
      <View style={styles.overviewContainer}>
        <Text style={styles.overviewTitle}>Overview</Text>
        <Text style={styles.overview}>{item.overview}</Text>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    // marginVertical: 5,
  },
  image: {
    height: 500,
    width: '100%',
  },

  details: {
    alignItems: 'center',
    marginVertical: 10,
  },

  title: {
    fontSize: 20,
    fontWeight: '700',
  },
  overviewContainer: {
    alignItems: 'center',
    marginVertical: 10,
  },
  overviewTitle: {
    fontWeight: '700',
    fontSize: 18,
  },
  overview: {
    paddingHorizontal: 10,
  },
});

export default DetailScreen;
