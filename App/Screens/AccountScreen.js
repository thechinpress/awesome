import React, {useContext} from 'react';
import {Text, View, TouchableOpacity, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import ProfileItem from '../components/ProfileItem';
import Context from '../Auths/Context';
import Storage from '../Auths/Storage';

function AccountScreen() {
  const {user, isLogin, setSkip, setIsLogin} = useContext(Context);

  const handleRegister = async () => {
    setSkip(false);
  };

  const handleLogout = async () => {
    setIsLogin(false);
    await Storage.removeData('userLogin');
  };

  return (
    <View style={styles.container}>
      <ProfileItem
        name={isLogin ? user.name : 'Sign up'}
        email={isLogin ? user.email : ''}
        onPress={handleRegister}
      />
      {isLogin && (
        <TouchableOpacity style={styles.logoutContainer} onPress={handleLogout}>
          <View style={styles.iconBackground}>
            <Icon name="lock" color="#fff" size={25} />
          </View>
          <Text style={styles.logout}>Log out</Text>
        </TouchableOpacity>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: 10,
    flex: 1,
  },
  logoutContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    position: 'absolute',
    bottom: 60,
    right: 20,
  },
  iconBackground: {
    alignItems: 'center',
    backgroundColor: '#000',
    borderRadius: 50,
    height: 40,
    justifyContent: 'center',
    width: 40,
  },

  logout: {
    paddingLeft: 10,
  },
});

export default AccountScreen;
