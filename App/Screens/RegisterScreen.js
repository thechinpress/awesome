import React, {useState, useContext, useEffect} from 'react';
import {Text, View, StyleSheet, Alert} from 'react-native';

import FormInput from '../components/FormInput';
import AppButton from '../components/AppButton';
import AccountOption from '../components/AccountOption';
import LoginSkip from '../components/LoginSkip';
import Context from '../Auths/Context';
import Storage from '../Auths/Storage';
import Loading from '../components/Loading';

function RegisterScreen(props) {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [cpassword, setCpassword] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const {isRegistered, setSkip, setUser, setIsRegistered} = useContext(Context);

  const handleSkip = async () => {
    setSkip(true);
  };

  const handlerRegister = async () => {
    if (name === '' || email === '' || password === '') {
      Alert.alert('Warning!', 'Please fill the required field!', [
        {text: 'Ok'},
      ]);
      return;
    }

    setIsLoading(true);
    const userInfo = {name, email, password};
    await Storage.storeData('userProfile', userInfo);
    setIsRegistered(true);
    setIsLoading(false);
  };

  useEffect(() => {
    const demo = async () => {
      const value = await Storage.getData('userProfile');
      setUser(value);
    };
    demo();
  }, [isRegistered]);

  const handleToLogin = () => {
    setIsRegistered(true);
  };

  return (
    <View style={styles.container}>
      <LoginSkip name="Skip" onPress={handleSkip} />

      <Text style={styles.register}>Register</Text>
      <FormInput
        placeholder="Username"
        icon="user"
        onChangeText={name => setName(name)}
      />
      <FormInput
        placeholder="Email"
        icon="envelope"
        onChangeText={email => setEmail(email)}
        keyboardType="email-address"
      />
      <FormInput
        placeholder="Password"
        icon="lock"
        onChangeText={pass => setPassword(pass)}
        secureTextEntry={true}
      />
      <FormInput
        placeholder="Confirm Password"
        icon="lock"
        onChangeText={cpass => setCpassword(cpass)}
        secureTextEntry={true}
      />
      {isLoading ? (
        <Loading size="small" />
      ) : (
        <AppButton title="Register" onPress={handlerRegister} />
      )}
      <AccountOption
        text="Already have an account?"
        title="Login"
        onPress={handleToLogin}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    margin: 15,
  },

  register: {
    fontSize: 25,
  },
});

export default RegisterScreen;
