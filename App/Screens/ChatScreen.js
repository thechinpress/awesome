import React, {useEffect, useState} from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import FormInput from '../components/FormInput';

function ChatScreen(props) {
  return (
    <View style={styles.container}>
      <View style={styles.chatTextLeft}>
        <Text style={styles.chatText}>Hello</Text>
      </View>
      <View style={styles.chatTextRight}>
        <Text style={styles.chatText}>Hi</Text>
      </View>

      <View style={styles.innerContainer}>
        <FormInput placeholder="Type" style={styles.input} />
        <TouchableOpacity
          style={styles.iconContainer}
          onPress={() => console.log('Sent!')}>
          <Icon name="send" size={30} color="gray" />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-end',
    marginHorizontal: 10,
  },

  chatTextLeft: {
    alignItems: 'flex-start',
  },

  chatTextRight: {
    alignItems: 'flex-end',
    marginBottom: 4,
  },

  chatText: {
    padding: 10,
    backgroundColor: '#fff',
    borderRadius: 10,
  },

  innerContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  input: {
    color: '#000',
    width: '90%',
    paddingHorizontal: 10,
  },
});

export default ChatScreen;
