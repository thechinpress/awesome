import React, {useEffect, useState} from 'react';
import {
  Linking,
  FlatList,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import HorizontalCard from '../components/HorizontalCard';
import VerticalCard from '../components/VerticalCard';
import ApiFetch from '../API/ApiFetch';
import Loading from '../components/Loading';

function HomeScreen({navigation}) {
  const [popularMovies, setPopularMovies] = useState([]);
  const [latestMovies, setLatestMovies] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [pageIsLoading, setPageIsLoading] = useState(false);
  const [pageNumber, setPageNumber] = useState();
  const [currentPage, setCurrentPage] = useState();
  const [totalPage, setTotalPage] = useState();

  // const [visible, setVisible] = useState(false);

  const {fetchPopularMovies} = ApiFetch();

  const api_key = '59e41d5ff20958e417b47e1acc53eea3';
  const url = 'https://www.themoviedb.org';

  useEffect(() => {
    const fetchLatestMoveis = () => {
      setPageIsLoading(true);
      fetch(
        `https://api.themoviedb.org/3/discover/movie?with_genres=18&primary_release_year=2021&api_key=${api_key}&page=${pageNumber}`,
      )
        .then(res => res.json())
        .then(data => {
          setLatestMovies(data.results);
          setTotalPage(data.total_pages);
          setPageNumber(data.page);
          setCurrentPage(data.page);
          setPageIsLoading(false);
        });
    };
    fetchLatestMoveis();
  }, [pageNumber]);

  const nextPage = () => {
    if (totalPage > pageNumber) {
      setPageNumber(prev => prev + 1);
      setCurrentPage(prev => prev + 1);
    }
  };

  const previousPage = () => {
    if (currentPage > 1) {
      setPageNumber(prev => prev - 1);
      setCurrentPage(prev => prev - 1);
    }
  };

  const loadPopularMovies = async () => {
    setIsLoading(true);
    const response = await fetchPopularMovies();
    setPopularMovies(response.results);
    setIsLoading(false);
  };

  useEffect(() => {
    loadPopularMovies();
  }, []);

  const openUrl = async () => {
    await Linking.openURL(url);
  };

  if (isLoading) {
    return <Loading size="large" />;
  }
  const imageUrl = 'https://image.tmdb.org/t/p/w500/';
  return (
    <View style={styles.container}>
      <Text style={styles.movieTitle}>Popular Movies</Text>

      {/* {visible && (
        <TouchableOpacity style={styles.icon}>
          <Icon name="chevron-right" size={30} color="lightgray" />
        </TouchableOpacity>
      )} */}

      <FlatList
        data={popularMovies}
        horizontal
        keyExtractor={item => item.id}
        // onEndReached={() => setVisible(true)}
        renderItem={({item}) => (
          <HorizontalCard
            title={item.original_title}
            image={imageUrl + item.poster_path}
            onPress={() => navigation.navigate('Detail', {item})}
          />
        )}
      />

      <View style={styles.verticalContainer}>
        <Text style={styles.movieTitle}>Latest Movies</Text>
        <View style={styles.innerVerticalContainer}>
          {pageIsLoading ? (
            <Loading size="small" />
          ) : (
            <FlatList
              numColumns={3}
              onEndReached={() => {
                setPageNumber(prev => prev + 1);
                setCurrentPage(prev => prev + 1);
              }}
              data={latestMovies}
              keyExtractor={item => item.id}
              renderItem={({item}) => (
                <VerticalCard
                  title={item.original_title}
                  image={imageUrl + item.poster_path}
                  onPress={() => navigation.navigate('Detail', {item})}
                />
              )}
            />
          )}
        </View>
        <View style={styles.paganition}>
          <TouchableOpacity onPress={previousPage}>
            <Icon
              name="chevron-circle-left"
              size={30}
              color={currentPage == 1 ? 'lightgray' : 'gray'}
            />
          </TouchableOpacity>
          <View style={styles.currentPageContainer}>
            <Text style={styles.currentPage}>{currentPage}</Text>
          </View>
          <TouchableOpacity onPress={nextPage}>
            <Icon
              name="chevron-circle-right"
              size={30}
              color={currentPage == totalPage ? 'lightgray' : 'gray'}
            />
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.copyRight}>
        <Text>All the materials used in this app are attributed to </Text>
        <TouchableOpacity style={styles.linking} onPress={openUrl}>
          <Text style={styles.textLink}>Themoviedb</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    margin: 5,
  },

  movieTitle: {
    fontSize: 20,
    padding: 10,
  },

  // icon: {
  //   position: 'absolute',
  //   top: 110,
  //   right: 10,
  //   zIndex: 1,
  // },

  verticalContainer: {
    height: 270,
    marginVertical: 5,
  },

  innerVerticalContainer: {
    height: 175,
  },

  paganition: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginTop: 5,
    marginRight: 10,
  },

  currentPageContainer: {
    alignItems: 'center',
    backgroundColor: 'lightgray',
    borderRadius: 20,

    marginHorizontal: 20,
  },

  currentPage: {
    color: '#0eb7f0',
    paddingHorizontal: 8,
    paddingVertical: 3,
  },

  copyRight: {
    alignItems: 'center',
  },

  linking: {
    marginTop: 10,
  },

  textLink: {
    color: '#0eb7f0',
  },
});

export default HomeScreen;
