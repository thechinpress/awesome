import React, {useEffect, useState} from 'react';
import {View, StyleSheet} from 'react-native';
import './App/redux';

import AppNavigation from './App/navigations/AppNavigation';
import AuthNavigator from './App/navigations/AuthNavigator';
import Context from './App/Auths/Context';
import Storage from './App/Auths/Storage';
// import Loading from './App/components/Loading';

const App = () => {
  const [user, setUser] = useState();
  const [skip, setSkip] = useState(false);
  const [isLogin, setIsLogin] = useState(false);
  const [isRegistered, setIsRegistered] = useState(false);
  // const [isLoading, setIsLoading] = useState(false);

  if (skip) {
    const storeSkip = async () => {
      try {
        await Storage.storeData('skip', skip);
      } catch (error) {
        console.log(error);
      }
    };
    storeSkip();
  }

  useEffect(() => {
    const getSkip = async () => {
      // setIsLoading(true);
      const value = await Storage.getData('skip');
      setSkip(value);
      // setIsLoading(false);
    };
    getSkip();
  }, []);

  // if (isLoading) {
  //   return (
  //     <View style={styles.loading}>
  //       <Loading size="large" />
  //     </View>
  //   );
  // }

  return (
    <View style={styles.screen}>
      <Context.Provider
        value={{
          user,
          skip,
          isLogin,
          isRegistered,
          setIsRegistered,
          setUser,
          setSkip,
          setIsLogin,
        }}>
        {isLogin || skip ? <AppNavigation /> : <AuthNavigator />}
      </Context.Provider>
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    backgroundColor: '#f1f1f1',
    flex: 1,
  },

  loading: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
});

export default App;
